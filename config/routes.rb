# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :leagues, only: :index
      resources :matches, only: :index
    end
  end
  get '*unmatched_route', to: 'application#raise_not_found'
end
