# frozen_string_literal: true

ActionController::Renderers.add :protobuf do |object, _options|
  send_data object, type: Mime[:protobuf], disposition: :inline
end
