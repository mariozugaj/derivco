# frozen_string_literal: true

class Rack::Attack
  Rack::Attack.cache.store = ActiveSupport::Cache::MemoryStore.new

  safelist('allow-localhost') do |req|
    req.ip == '127.0.0.1' || req.ip == '::1'
  end

  throttle('req/ip', limit: 60, period: 1.minute, &:ip)

  self.throttled_response = lambda do |env|
    retry_after = (env['rack.attack.match_data'] || {})[:period]

    [
      429,
      {
        'Content-Type' => 'application/json', 'Retry-After' => retry_after.to_s
      },
      [{ error: 'Throttle limit reached. Retry later.' }.to_json]
    ]
  end
end
