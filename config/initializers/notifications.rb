# frozen_string_literal: true

ActiveSupport::Notifications.subscribe(
  'rack.attack'
) do |_name, _start, _finish, _request_id, request|
  request = request[:request]
  if request.env['rack.attack.match_type'] == :throttle
    Rails.logger.info(
      "#{request.ip_address} has been rate limited for url #{request.url}"
    )
  end
end
