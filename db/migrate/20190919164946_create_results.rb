class CreateResults < ActiveRecord::Migration[6.0]
  def change
    create_table :results do |t|
      t.integer :fthg, null: false
      t.integer :ftag, null: false
      t.string :ftr, null: false
      t.integer :hthg, null: false
      t.integer :htag, null: false
      t.string :htr, null: false
      t.references :match, foreign_key: true, null: false

      t.timestamps
    end
  end
end
