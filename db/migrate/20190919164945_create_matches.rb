class CreateMatches < ActiveRecord::Migration[6.0]
  def change
    create_table :matches do |t|
      t.references :league, foreign_key: true, null: false
      t.references :season, foreign_key: true, null: false
      t.integer :home_team_id, null: false
      t.integer :away_team_id, null: false
      t.datetime :date, index: true, null: false

      t.timestamps
    end
  end
end
