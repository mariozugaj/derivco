class CreateSeasons < ActiveRecord::Migration[6.0]
  def change
    create_table :seasons do |t|
      t.string :period, index: true, null: false
      t.references :league, foreign_key: true, null: false

      t.timestamps
    end
  end
end
