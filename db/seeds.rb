# frozen_string_literal: true

require 'csv'

CSV.foreach('data/data.csv', headers: true) do |row|
  league = League.where(name: row[1]).first_or_create
  season = Season.where(period: row[2], league: league).first_or_create
  home_team = Team.where(name: row[4]).first_or_create
  away_team = Team.where(name: row[5]).first_or_create
  season_match =
    Match.where(
      league: league,
      season: season,
      home_team: home_team,
      away_team: away_team,
      date: Date.parse(row[3])
    ).first_or_create
  Result.where(
    match: season_match,
    fthg: row[6],
    ftag: row[7],
    ftr: row[8],
    hthg: row[9],
    htag: row[10],
    htr: row[11]
  ).first_or_create
end
