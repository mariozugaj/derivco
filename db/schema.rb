# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_19_164946) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "leagues", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_leagues_on_name"
  end

  create_table "matches", force: :cascade do |t|
    t.bigint "league_id", null: false
    t.bigint "season_id", null: false
    t.integer "home_team_id", null: false
    t.integer "away_team_id", null: false
    t.datetime "date", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["date"], name: "index_matches_on_date"
    t.index ["league_id"], name: "index_matches_on_league_id"
    t.index ["season_id"], name: "index_matches_on_season_id"
  end

  create_table "results", force: :cascade do |t|
    t.integer "fthg", null: false
    t.integer "ftag", null: false
    t.string "ftr", null: false
    t.integer "hthg", null: false
    t.integer "htag", null: false
    t.string "htr", null: false
    t.bigint "match_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["match_id"], name: "index_results_on_match_id"
  end

  create_table "seasons", force: :cascade do |t|
    t.string "period", null: false
    t.bigint "league_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["league_id"], name: "index_seasons_on_league_id"
    t.index ["period"], name: "index_seasons_on_period"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_teams_on_name"
  end

  add_foreign_key "matches", "leagues"
  add_foreign_key "matches", "seasons"
  add_foreign_key "results", "matches"
  add_foreign_key "seasons", "leagues"
end
