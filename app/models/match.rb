# frozen_string_literal: true

# Match model
class Match < ApplicationRecord
  belongs_to :league
  belongs_to :season
  belongs_to :home_team, class_name: 'Team'
  belongs_to :away_team, class_name: 'Team'
  has_one :result, dependent: :destroy

  validates_presence_of :date, :league, :season, :home_team, :away_team
end
