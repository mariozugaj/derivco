# frozen_string_literal: true

# Result model
class Result < ApplicationRecord
  belongs_to :match

  validates_presence_of :fthg, :ftag, :ftr, :hthg, :htag, :htr, :match
end
