# frozen_string_literal: true

# League model
class League < ApplicationRecord
  has_many :seasons, dependent: :destroy
  has_many :matches

  validates_presence_of :name
end
