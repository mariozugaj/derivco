# frozen_string_literal: true

# Season model
class Season < ApplicationRecord
  belongs_to :league
  has_many :matches, dependent: :destroy

  validates_presence_of :period, :league
end
