# frozen_string_literal: true

# Decorator for Season model
class SeasonDecorator < BaseDecorator
  delegate :period, to: :object

  # Takes in period in format '201617', and returns '2016-2017'
  def formatted_period
    millenium = period[0..1]
    period_first_year = period[-4..-3]
    period_second_year = period[-2..-1]
    "#{millenium}#{period_first_year}-#{millenium}#{period_second_year}"
  end
end
