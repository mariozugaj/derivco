# frozen_string_literal: true

# Decorator for League model
class LeagueDecorator < BaseDecorator
  delegate :name, to: :object

  def long_name
    case name
    when 'SP1'
      'La Liga'
    when 'SP2'
      'La Liga2'
    when 'E0'
      'Premier'
    when 'D1'
      'Bundesliga'
    else
      name
    end
  end
end
