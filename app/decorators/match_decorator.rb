# frozen_string_literal: true

# Decorator for Match model
class MatchDecorator < BaseDecorator
  def formatted_date
    object.date.strftime('%e/%m/%Y')
  end
end
