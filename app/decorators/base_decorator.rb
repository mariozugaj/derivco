# frozen_string_literal: true

# Base decorator class
class BaseDecorator < SimpleDelegator
  attr_reader :object

  def initialize(object)
    @object = object
  end
end
