# frozen_string_literal: true

# Query for leagues with matches present
class LeaguesWithMatches
  def result
    League.where(id: Match.select(:league_id).distinct)
  end
end
