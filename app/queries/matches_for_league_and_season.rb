# frozen_string_literal: true

# Query for matches for given league and season
class MatchesForLeagueAndSeason
  attr_reader :search_params

  def initialize(search_params)
    @search_params = search_params
  end

  def result
    if search_params.blank?
      Match.all
    else
      Match.joins(%i[league season]).where(
        leagues: { name: search_params[:league] },
        seasons: { period: search_params[:season] }
      )
    end
  end
end
