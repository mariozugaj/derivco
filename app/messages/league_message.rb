# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: league_message.proto

require 'google/protobuf'

require 'season_collection_message'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("league_message.proto", :syntax => :proto3) do
    add_message "LeagueMessage" do
      optional :id, :int32, 1
      optional :name, :string, 2
      optional :long_name, :string, 3
      optional :seasons, :message, 4, "SeasonCollectionMessage"
    end
  end
end

LeagueMessage = Google::Protobuf::DescriptorPool.generated_pool.lookup("LeagueMessage").msgclass
