# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: season_collection_message.proto

require 'google/protobuf'

require 'season_message'
Google::Protobuf::DescriptorPool.generated_pool.build do
  add_file("season_collection_message.proto", :syntax => :proto3) do
    add_message "SeasonCollectionMessage" do
      repeated :seasons, :message, 1, "SeasonMessage"
    end
  end
end

SeasonCollectionMessage = Google::Protobuf::DescriptorPool.generated_pool.lookup("SeasonCollectionMessage").msgclass
