# frozen_string_literal: true

module API
  module V1
    # Matches controller
    class MatchesController < ApplicationController
      before_action :set_matches

      # GET /api/v1/matches
      def index
        respond_to do |format|
          format.json do
            json_response(
              MatchSerializer.new(
                @matches,
                include: %i[result home_team away_team]
              )
                .serialized_json
            )
          end
          format.protobuf do
            protobuf_response(
              MatchCollectionProtobufSerializer.new(@matches).encode
            )
          end
        end
      end

      private

      def search_params
        params.permit(:league, :season)
      end

      def set_matches
        @matches =
          paginate MatchesForLeagueAndSeason.new(search_params).result.includes(
                     %i[result home_team away_team]
                   )
      end
    end
  end
end
