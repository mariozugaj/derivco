# frozen_string_literal: true

module API
  module V1
    # Leagues controller
    class LeaguesController < ApplicationController
      # GET /api/v1/leagues
      def index
        leagues = paginate LeaguesWithMatches.new.result.includes(:seasons)
        respond_to do |format|
          format.json do
            json_response(
              LeagueSerializer.new(leagues, include: %i[seasons])
                .serialized_json
            )
          end
          format.protobuf do
            protobuf_response(
              LeagueCollectionProtobufSerializer.new(leagues).encode
            )
          end
        end
      end
    end
  end
end
