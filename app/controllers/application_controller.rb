# frozen_string_literal: true

class ApplicationController < ActionController::API
  include ActionController::MimeResponds
  include Response
  include ExceptionHandler

  before_action :set_raven_context

  def raise_not_found
    raise ActionController::RoutingError,
          "No route matches #{params[:unmatched_route]}"
  end

  private

  def set_raven_context
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end
end
