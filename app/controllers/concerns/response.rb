# frozen_string_literal: true

# Different API response formats
module Response
  extend ActiveSupport::Concern

  def json_response(object, status = :ok)
    render json: object, status: status
  end

  def protobuf_response(object, status = :ok)
    render protobuf: object, status: status
  end
end
