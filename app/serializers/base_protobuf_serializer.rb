# frozen_string_literal: true

# Base protobuf serializer
class BaseProtobufSerializer
  attr_reader :object

  def initialize(object)
    @object = object
  end

  def encode
    encoded_message
  end

  def decode
    message_class.decode(encoded_message)
  end

  def message
    raise NotImplementedError,
          'Please provide a protobuf message object to serialize'
  end

  private

  def encoded_message
    @encoded_message ||= message_class.encode(message)
  end

  def message_class
    @message_class ||=
      "#{self.class.name.remove('ProtobufSerializer')}Message".constantize
  end
end
