# frozen_string_literal: true

# Season collection protobuf serializer
class SeasonCollectionProtobufSerializer < BaseProtobufSerializer
  def message
    message_class.new(seasons: season_messages)
  end

  private

  def season_messages
    object.map { |season| SeasonProtobufSerializer.new(season).message }
  end
end
