# frozen_string_literal: true

# Result protobuf serializer
class ResultProtobufSerializer < BaseProtobufSerializer
  def message
    message_class.new(
      id: object.id,
      fthg: object.fthg,
      ftag: object.ftag,
      ftr: object.ftr,
      hthg: object.hthg,
      htag: object.htag,
      htr: object.htr
    )
  end
end
