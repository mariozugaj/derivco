# frozen_string_literal: true

# League protobuf serializer
class LeagueProtobufSerializer < BaseProtobufSerializer
  def message
    message_class.new(
      id: object.id,
      name: object.name,
      long_name: LeagueDecorator.new(object).long_name,
      seasons: season_messages
    )
  end

  def season_messages
    SeasonCollectionProtobufSerializer.new(object.seasons).message
  end
end
