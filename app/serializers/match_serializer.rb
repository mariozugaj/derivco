# frozen_string_literal: true

# Match serializer
class MatchSerializer
  include FastJsonapi::ObjectSerializer

  attributes :date do |match|
    MatchDecorator.new(match).formatted_date
  end

  has_one :result
  belongs_to :home_team, record_type: :team
  belongs_to :away_team, record_type: :team

  cache_options enabled: true
end
