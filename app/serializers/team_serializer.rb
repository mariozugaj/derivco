# frozen_string_literal: true

# Team serializer
class TeamSerializer
  include FastJsonapi::ObjectSerializer

  attribute :name
  cache_options enabled: true
end
