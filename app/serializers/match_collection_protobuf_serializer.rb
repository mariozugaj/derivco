# frozen_string_literal: true

# Match collection protobuf serializer
class MatchCollectionProtobufSerializer < BaseProtobufSerializer
  def message
    message_class.new(matches: match_messages)
  end

  def match_messages
    object.map { |match| MatchProtobufSerializer.new(match).message }
  end
end
