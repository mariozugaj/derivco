# frozen_string_literal: true

# Season serializer\
class SeasonSerializer
  include FastJsonapi::ObjectSerializer

  attribute :period
  attribute :formatted_period do |object|
    SeasonDecorator.new(object).formatted_period
  end

  cache_options enabled: true
end
