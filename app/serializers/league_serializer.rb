# frozen_string_literal: true

# League serializer
class LeagueSerializer
  include FastJsonapi::ObjectSerializer

  attribute :name
  attribute :long_name do |object|
    LeagueDecorator.new(object).long_name
  end

  has_many :seasons

  cache_options enabled: true
end
