# frozen_string_literal: true

# Result serializer
class ResultSerializer
  include FastJsonapi::ObjectSerializer

  attributes :fthg, :ftag, :ftr, :hthg, :htag, :htr
  cache_options enabled: true
end
