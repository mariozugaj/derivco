# frozen_string_literal: true

# Team protobuf serializer
class TeamProtobufSerializer < BaseProtobufSerializer
  def message
    message_class.new(id: object.id, name: object.name)
  end
end
