# frozen_string_literal: true

# League collection protobuf serializer
class LeagueCollectionProtobufSerializer < BaseProtobufSerializer
  def message
    message_class.new(leagues: league_messages)
  end

  private

  def league_messages
    object.map { |league| LeagueProtobufSerializer.new(league).message }
  end
end
