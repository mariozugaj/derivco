# frozen_string_literal: true

# Season protobuf serializer
class SeasonProtobufSerializer < BaseProtobufSerializer
  def message
    message_class.new(
      id: object.id,
      period: object.period,
      formatted_period: SeasonDecorator.new(object).formatted_period
    )
  end
end
