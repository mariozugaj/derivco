# frozen_string_literal: true

# Match protobuf serializer
class MatchProtobufSerializer < BaseProtobufSerializer
  def message
    message_class.new(
      id: object.id,
      date: MatchDecorator.new(object).formatted_date,
      home_team: home_team_message,
      away_team: away_team_message,
      result: result_message
    )
  end

  def home_team_message
    TeamProtobufSerializer.new(object.home_team).message
  end

  def away_team_message
    TeamProtobufSerializer.new(object.away_team).message
  end

  def result_message
    ResultProtobufSerializer.new(object.result).message
  end
end
