# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MatchSerializer, type: :serializer do
  describe 'serialization' do
    let(:subject) { described_class.new(match).serializable_hash }
    let(:home_team) { create(:team) }
    let(:away_team) { create(:team) }
    let(:match) do
      create(
        :match,
        date: '22/09/2019', away_team: away_team, home_team: home_team
      )
    end
    let(:attributes) { subject[:data][:attributes] }
    let(:relationships) { subject[:data][:relationships] }

    before { create(:result, match: match) }

    it 'serialises proper attributes' do
      expect(subject[:data][:type]).to eq :match
      expect(attributes[:date]).to eq '22/09/2019'
      expect(relationships[:result][:data][:id]).to eq match.result.id.to_s
      expect(relationships[:home_team][:data][:id]).to eq home_team.id.to_s
      expect(relationships[:away_team][:data][:id]).to eq away_team.id.to_s
    end
  end
end
