# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TeamProtobufSerializer, type: :serializer do
  describe 'serialization' do
    let(:subject) { described_class.new(team) }
    let(:team) { create(:team, name: 'Derivco') }
    let(:encoded_message) { subject.encode }
    let(:decoded_message) { subject.decode }

    context 'encoded message' do
      it 'is of class String' do
        expect(encoded_message.class).to eq String
      end
    end

    context 'decoded message' do
      it 'contains proper attributes' do
        expect(decoded_message.name).to eq 'Derivco'
      end
    end
  end
end
