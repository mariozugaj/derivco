# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SeasonSerializer, type: :serializer do
  describe 'serialization' do
    let(:subject) { described_class.new(season).serializable_hash }
    let(:season) { create(:season, period: '201920') }
    let(:attributes) { subject[:data][:attributes] }

    it 'serialises proper attributes' do
      expect(subject[:data][:type]).to eq :season
      expect(attributes[:period]).to eq '201920'
      expect(attributes[:formatted_period]).to eq '2019-2020'
    end
  end
end
