# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SeasonCollectionProtobufSerializer, type: :serializer do
  describe 'serialization' do
    let(:subject) { described_class.new(seasons) }
    let(:seasons) { create_list(:season, 5) }
    let(:encoded_message) { subject.encode }
    let(:decoded_message) { subject.decode }

    context 'encoded message' do
      it 'is of class String' do
        expect(encoded_message.class).to eq String
      end
    end

    context 'decoded message' do
      it 'contains match messages' do
        expect(decoded_message.to_h[:seasons]).not_to be_empty
        expect(decoded_message.to_h[:seasons].size).to eq 5
      end
    end
  end
end
