# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MatchProtobufSerializer, type: :serializer do
  describe 'serialization' do
    let(:subject) { described_class.new(match) }
    let(:home_team) { create(:team) }
    let(:away_team) { create(:team) }
    let(:match) do
      create(
        :match,
        date: '22/09/2019', away_team: away_team, home_team: home_team
      )
    end
    let(:encoded_message) { subject.encode }
    let(:decoded_message) { subject.decode }

    before { create(:result, match: match) }

    context 'encoded message' do
      it 'is of class String' do
        expect(encoded_message.class).to eq String
      end
    end

    context 'decoded message' do
      it 'contains proper attributes' do
        expect(decoded_message.date).to eq '22/09/2019'
        expect(decoded_message.home_team.id).to eq home_team.id
        expect(decoded_message.away_team.id).to eq away_team.id
        expect(decoded_message.result.id).to eq match.result.id
      end
    end
  end
end
