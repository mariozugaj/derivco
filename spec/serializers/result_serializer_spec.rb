# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ResultSerializer, type: :serializer do
  describe 'serialization' do
    let(:subject) { described_class.new(result).serializable_hash }
    let(:result) do
      create(:result, fthg: 0, ftag: 0, ftr: 'A', hthg: 0, htag: 0, htr: 'D')
    end
    let(:attributes) { subject[:data][:attributes] }

    it 'serialises proper attributes' do
      expect(subject[:data][:type]).to eq :result
      expect(attributes[:fthg]).to eq 0
      expect(attributes[:ftag]).to eq 0
      expect(attributes[:ftr]).to eq 'A'
      expect(attributes[:hthg]).to eq 0
      expect(attributes[:htag]).to eq 0
      expect(attributes[:htr]).to eq 'D'
    end
  end
end
