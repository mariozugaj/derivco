# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LeagueProtobufSerializer, type: :serializer do
  describe 'serialization' do
    let(:subject) { described_class.new(league) }
    let(:league) { create(:league, name: 'SP1') }
    let(:encoded_message) { subject.encode }
    let(:decoded_message) { subject.decode }

    before { create_list(:season, 5, league: league) }

    context 'encoded message' do
      it 'is of class String' do
        expect(encoded_message.class).to eq String
      end
    end

    context 'decoded message' do
      it 'contains proper attributes' do
        expect(decoded_message.name).to eq 'SP1'
        expect(decoded_message.long_name).to eq 'La Liga'
        expect(decoded_message.to_h[:seasons][:seasons]).not_to be_empty
        expect(decoded_message.to_h[:seasons][:seasons].size).to eq 5
      end
    end
  end
end
