# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LeagueSerializer, type: :serializer do
  describe 'serialization' do
    let(:subject) { described_class.new(league).serializable_hash }
    let(:seasons) { create_list(:season, 5) }
    let(:league) { create(:league, name: 'SP1', seasons: seasons) }
    let(:attributes) { subject[:data][:attributes] }

    it 'serialises proper attributes' do
      expect(subject[:data][:type]).to eq :league
      expect(attributes[:name]).to eq 'SP1'
      expect(attributes[:long_name]).to eq 'La Liga'
      expect(subject[:data][:relationships][:seasons][:data].size).to eq 5
    end
  end
end
