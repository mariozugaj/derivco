# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ResultProtobufSerializer, type: :serializer do
  describe 'serialization' do
    let(:subject) { described_class.new(result) }
    let(:result) do
      create(:result, fthg: 0, ftag: 0, ftr: 'A', hthg: 0, htag: 0, htr: 'D')
    end
    let(:encoded_message) { subject.encode }
    let(:decoded_message) { subject.decode }

    context 'encoded message' do
      it 'is of class String' do
        expect(encoded_message.class).to eq String
      end
    end

    context 'decoded message' do
      it 'contains proper attributes' do
        expect(decoded_message.fthg).to eq 0
        expect(decoded_message.ftag).to eq 0
        expect(decoded_message.ftr).to eq 'A'
        expect(decoded_message.hthg).to eq 0
        expect(decoded_message.htag).to eq 0
        expect(decoded_message.htr).to eq 'D'
      end
    end
  end
end
