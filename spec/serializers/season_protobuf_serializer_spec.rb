# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SeasonProtobufSerializer, type: :serializer do
  describe 'serialization' do
    let(:subject) { described_class.new(season) }
    let(:season) { create(:season, period: '201920') }
    let(:encoded_message) { subject.encode }
    let(:decoded_message) { subject.decode }

    context 'encoded message' do
      it 'is of class String' do
        expect(encoded_message.class).to eq String
      end
    end

    context 'decoded message' do
      it 'contains proper attributes' do
        expect(decoded_message.period).to eq '201920'
        expect(decoded_message.formatted_period).to eq '2019-2020'
      end
    end
  end
end
