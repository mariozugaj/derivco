# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TeamSerializer, type: :serializer do
  describe 'serialization' do
    let(:subject) { described_class.new(team).serializable_hash }
    let(:team) { create(:team, name: 'Derivco') }
    let(:attributes) { subject[:data][:attributes] }

    it 'serialises proper attributes' do
      expect(subject[:data][:type]).to eq :team
      expect(attributes[:name]).to eq 'Derivco'
    end
  end
end
