# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LeaguesWithMatches, type: :query do
  let(:subject) { described_class.new }

  describe 'result' do
    context 'with leagues with matches' do
      before { create_list(:league_with_matches, 5) }

      it 'returns leagues with matches' do
        expect(subject.result).not_to be_empty
        expect(subject.result.size).to eq 5
      end
    end

    context 'without leagues with matches' do
      before { create_list(:league, 5) }

      it 'returns empty result' do
        expect(subject.result).to be_empty
      end
    end
  end
end
