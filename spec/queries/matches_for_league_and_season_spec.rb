# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MatchesForLeagueAndSeason, type: :query do
  let(:subject) { described_class.new(search_params) }

  describe 'result' do
    context 'with no search params' do
      before { create_list(:match, 5) }

      let(:search_params) { {} }

      it 'returns all matches' do
        expect(subject.result).not_to be_empty
        expect(subject.result.size).to eq 5
      end
    end

    context 'with search params' do
      let(:league) { create(:league, name: 'SP1') }
      let(:season) { create(:season, period: '201920') }
      let(:search_params) { { league: 'SP1', season: '201920' }}

      before do
        create_list(:match, 5)
        create_list(:match, 5, league: league, season: season)
      end

      it 'returns all matches that match the params' do
        expect(subject.result).not_to be_empty
        expect(subject.result.size).to eq 5
      end
    end

    context 'no matches available' do
      let(:search_params) { {} }

      it 'returns an empty result' do
        expect(subject.result).to be_empty
      end
    end
  end
end
