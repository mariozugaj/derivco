# frozen_string_literal: true

require 'rails_helper'

RSpec.describe League, type: :model do
  it { should have_many(:seasons).dependent(:destroy) }
  it { should have_many(:matches) }

  it { should validate_presence_of(:name) }
end
