# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Result, type: :model do
  it { should belong_to(:match) }

  it { should validate_presence_of(:fthg) }
  it { should validate_presence_of(:ftag) }
  it { should validate_presence_of(:ftr) }
  it { should validate_presence_of(:hthg) }
  it { should validate_presence_of(:htag) }
  it { should validate_presence_of(:htr) }
end
