# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Season, type: :model do
  it { should belong_to(:league) }
  it { should have_many(:matches).dependent(:destroy) }

  it { should validate_presence_of(:period) }
  it { should validate_presence_of(:league) }
end
