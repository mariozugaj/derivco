# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Match, type: :model do
  it { should belong_to(:league) }
  it { should belong_to(:season) }
  it { should belong_to(:home_team) }
  it { should belong_to(:away_team) }
  it { should have_one(:result).dependent(:destroy) }

  it { should validate_presence_of(:date) }
  it { should validate_presence_of(:league) }
  it { should validate_presence_of(:season) }
  it { should validate_presence_of(:home_team) }
  it { should validate_presence_of(:away_team) }
end
