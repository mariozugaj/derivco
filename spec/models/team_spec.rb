# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Team, type: :model do
  it { should have_many(:home_matches).class_name('Match') }
  it { should have_many(:away_matches).class_name('Match') }

  it { should validate_presence_of(:name) }
end
