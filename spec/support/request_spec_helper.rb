# frozen_string_literal: true

# Helpers for request tests
module RequestSpecHelper
  def json
    JSON.parse(response.body)
  end

  def json_headers
    { headers: { 'ACCEPT' => 'application/json' } }
  end

  def protobuf_headers
    { headers: { 'ACCEPT' => 'application/vnd.google.protobuf' } }
  end
end
