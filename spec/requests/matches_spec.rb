# frozen_string_literal: true

require 'rails_helper'
require 'support/request_spec_helper'

RSpec.describe 'Matches API', type: :request do
  include RequestSpecHelper

  let!(:matches) {}

  describe 'GET /api/v1/matches JSON' do
    before { get '/api/v1/matches', json_headers }

    context 'without matches' do
      it 'returns an empty response' do
        expect(json).not_to be_empty
        expect(json['data'].size).to eq(0)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'with less than 20 matches' do
      let!(:matches) { create_list(:match, 18) }

      it 'returns all matches' do
        expect(json).not_to be_empty
        expect(json['data'].size).to eq(18)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'sets total results to response headers' do
        expect(response.headers['Per-Page']).to eq '20'
        expect(response.headers['Total']).to eq '18'
      end

      it 'does not set page links in headers' do
        expect(response.headers['Link']).to be_nil
      end
    end

    context 'with more than 20 matches' do
      let!(:matches) { create_list(:match, 45) }

      it 'returns matches, max 20' do
        expect(json).not_to be_empty
        expect(json['data'].size).to eq(20)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'sets total results to response headers' do
        expect(response.headers['Per-Page']).to eq '20'
        expect(response.headers['Total']).to eq '45'
      end

      it 'does not set page links in headers' do
        expect(response.headers['Link']).not_to be_nil
      end
    end
  end

  describe 'GET /api/v1/matches PROTOBUF' do
    before { get '/api/v1/matches', protobuf_headers }

    context 'without matches' do
      it 'returns an empty response' do
        expect(response.body).to be_empty
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'with matches' do
      let!(:matches) { create_list(:match_with_result, 5) }
      let(:decoded_response) do
        MatchCollectionMessage.decode(response.body).to_h[:matches]
      end

      it 'returns all matches' do
        expect(decoded_response).not_to be_empty
        expect(decoded_response.size).to eq(5)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end
  end
end
