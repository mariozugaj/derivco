# frozen_string_literal: true

require 'rails_helper'
require 'support/request_spec_helper'

RSpec.describe 'API', type: :request do
  include RequestSpecHelper

  let(:json_headers) { { headers: { 'ACCEPT' => 'application/json' } } }

  context 'with no route found' do
    before { get '/api/route/that/does/not/exist', json_headers }

    it 'should return 404' do
      expect(response.status).to eq(404)
      expect(json['message']).to eq(
        'No route matches api/route/that/does/not/exist'
      )
    end
  end
end
