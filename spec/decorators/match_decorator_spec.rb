# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MatchDecorator, type: :decorator do
  describe '#formatted_date' do
    let(:match) { create(:match, date: Date.new(2019, 9, 22)) }
    let(:subject) { described_class.new(match) }

    it 'returns date in dd/mm/yyyy format' do
      expect(subject.formatted_date).to eq '22/09/2019'
    end
  end
end
