# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SeasonDecorator, type: :decorator do
  describe '#formatted_period' do
    let(:season) { create(:season, period: '201920') }
    let(:subject) { described_class.new(season) }

    it 'returns period in yyyy-yyyy format' do
      expect(subject.formatted_period).to eq '2019-2020'
    end
  end
end
