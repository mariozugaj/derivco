# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LeagueDecorator, type: :decorator do
  describe '#long_name' do
    let(:subject) { described_class.new(league) }

    context 'with mapped long name' do
      let(:league) { create(:league, name: 'D1') }

      it 'returns long name' do
        expect(subject.long_name).to eq 'Bundesliga'
      end
    end

    context 'without mapped long name' do
      let(:league) { create(:league, name: 'C1') }

      it 'returns name' do
        expect(subject.long_name).to eq 'C1'
      end
    end
  end
end
