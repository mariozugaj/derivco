# frozen_string_literal: true

FactoryBot.define do
  factory :result do
    fthg { Faker::Number.number(digits: 1) }
    ftag { Faker::Number.number(digits: 1) }
    ftr { %w[H A D].sample }
    hthg { Faker::Number.number(digits: 1) }
    htag { Faker::Number.number(digits: 1) }
    htr { %w[H A D].sample }
    association :match
  end
end
