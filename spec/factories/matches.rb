# frozen_string_literal: true

FactoryBot.define do
  factory :match do
    date do
      Faker::Date.between(
        from: Date.new(2_019, 9, 1), to: Date.new(2_019, 9, 30)
      )
    end
    league
    season
    association :home_team, factory: :team
    association :away_team, factory: :team

    factory :match_with_result do
      after(:create) { |match| create(:result, match: match) }
    end
  end
end
