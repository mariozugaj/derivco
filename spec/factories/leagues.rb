# frozen_string_literal: true

FactoryBot.define do
  factory :league do
    name { Faker::Lorem.word }

    factory :league_with_matches do
      after(:create) do |league|
        team = create(:team)
        season = create(:season, league: league)
        create(
          :match,
          league: league, season: season, home_team: team, away_team: team
        )
      end
    end
  end
end
