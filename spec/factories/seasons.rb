# frozen_string_literal: true

FactoryBot.define do
  factory :season do
    period { Faker::Number.number(digits: 6) }
    league
  end
end
