# Derivco football results API

This is a simple Ruby on Rails API application that serves football results.

With it, you can:

- list the league and season pairs (e.g. La Liga 2017-2018) for which there are results available,
- fetch the results for a specific league and season pair,
- retrieve the results formatted in JSON and Protocol Buffers.

## Requirements
- Docker version 19.03.2, or higher
- docker-compose version 1.24.1, or higher

Check your versions by running

```shell
docker -v
docker-compose -v
```

If you don't have them installed, head over to [Docker docs](https://docs.docker.com/docker-for-mac/) and follow installation instructions.

## Installing / Getting started

To start the application, run following commands:

```shell
git clone https://gitlab.com/mariozugaj/derivco.git
cd derivco/
docker pull mariozugaj/derivco
docker-compose up
docker-compose exec app bundle exec rails db:setup
```

This will do the following:
- clone the repository,
- pull prebuilt application image from Docker HUB,
- fetch and start services (app, database, balancer)
- create and seed the database (from `data.csv` file, it takes some time).

You can access the application at http://localhost:3000

## Using
Head over to API documentation ([link bellow ](#links)) and see examples there.

## Testing

Run tests by executing following:

```shell
docker-compose exec app bundle exec rspec
```
## Deploying / Publishing

Deploying to localhost (for now), with 3 app instances and a HAProxy load balancer.

```shell
docker stack deploy --compose-file=docker-compose.yml dev
```

HAProxy load balancer config:
- uses the dockercloud/haproxy:latest image
- automatically sets the haproxy.cfg
- exposes port 80
- connects to app intances on port 3000
- uses leastconn balancing mode

You can access the application at http://localhost.

## Building
To build it locally, run:

```shell
docker-compose up --build
```
## Links

- [Project homepage](https://gitlab.com/mariozugaj/derivco.git)
- [Repository](https://gitlab.com/mariozugaj/derivco.git)
- [Docker image](https://hub.docker.com/r/mariozugaj/derivco)
- [API documentation](https://documenter.getpostman.com/view/8855082/SVmySxvD?version=latest)


## Licensing

The code in this project is licensed under MIT license.