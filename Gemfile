# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.4'

gem 'api-pagination', '~> 4.8', '>= 4.8.2'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'fast_jsonapi', '~> 1.5'
gem 'google-protobuf', '~> 3.9', '>= 3.9.1'
gem 'pagy', '~> 3.5', '>= 3.5.1'
gem 'pg', '~> 1.1', '>= 1.1.4'
gem 'puma', '~> 3.11'
gem 'rack-attack', '~> 6.1'
gem 'rails', '~> 6.0.0'
gem 'sentry-raven', '~> 2.11', '>= 2.11.2'
gem 'skylight', '~> 4.1', '>= 4.1.2'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails', '~> 3.8', '>= 3.8.2'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'overcommit', '~> 0.49.1'
  gem 'rubocop', '~> 0.74.0'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'database_cleaner', '~> 1.7'
  gem 'factory_bot_rails', '~> 5.0', '>= 5.0.2'
  gem 'faker', '~> 2.3'
  gem 'shoulda-matchers', '~> 4.1', '>= 4.1.2'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
